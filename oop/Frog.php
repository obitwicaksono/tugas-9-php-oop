<?php

class Frog
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    function __construct (string $par_name) {
        $this->name = $par_name;
    }

    function jump() {
        echo "hop hop";  
    }
}

?>