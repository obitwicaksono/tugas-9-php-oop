<?php

class Ape
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";

    function __construct (string $par_name) {
        $this->name = $par_name;
    }

    function yell() {
        echo "Auooo";  
    }
}

?>